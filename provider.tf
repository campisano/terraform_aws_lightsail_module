provider "aws" {
  region                   = var.aws_provider.region
  shared_credentials_files = var.aws_provider.credentials_files
  profile                  = var.aws_provider.profile
}
